import { component$, $, useClientEffect$, useStore, useContext } from '@builder.io/qwik';
import { useLocation } from '@builder.io/qwik-city';
import type { DocumentHead } from '@builder.io/qwik-city';
import type { FormElement } from '../../../models/Form';
import { form } from '../../../data/event';
import Form from '../../../components/form/form';
import { getData, populateForm } from '../../../util/form';
import { USERS, EVENTS } from '../../../data/url'
import { GET, POST, PUT } from '../../../util/http';
import { Loader } from '../../../util/context';

export default component$(() => {
    const loader = useContext(Loader);
    const loc = useLocation();
    const store = useStore<{ form: FormElement[] }>({
        form: form
    }, { recursive: true });

    const submit = $(async (form: FormElement[]) => {
        try {
            if (loc.params.eventId && loc.params.eventId !== 'null') {
                await PUT(EVENTS + loc.params.eventId, getData(form), loader);
            } else {
                await POST(EVENTS, getData(form), loader);
            }
        } catch (error) {
            console.log(error)
        }
    });

    const mapUsers = $((response: any) => {
        const options = response.map((u: any) => ({ value: u.email, label: `${u.first_name} ${u.last_name}` }));
        const members = store.form.find(f => f.key === 'members');
        if (members) {
            members.options = options;
        }
    })

    useClientEffect$(async () => {
        try {
            if (loc.params.eventId && loc.params.eventId !== 'null') {
                const response = await Promise.all([GET(EVENTS + loc.params.eventId, loader), GET(USERS, loader)]);
                if(response[0]) {
                    store.form = populateForm(store.form, response[0])
                }
                mapUsers(response[1]);
            } else {
                const response: any = await GET(USERS, loader);
                mapUsers(response);
            }
        } catch (error) {
            console.log(error)
        }
    });
    return (
        <div>
            <Form layout={store.form} title={loc.params.eventId && loc.params.eventId !== 'null' ? 'Edit Event' : 'Create Event'} submit={submit} />
        </div>
    );
});

export const head: DocumentHead = {
    title: 'Event Management'
};