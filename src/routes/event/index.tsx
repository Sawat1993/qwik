import { component$, useStore, $, useClientEffect$, useContext } from '@builder.io/qwik';
import { useNavigate } from '@builder.io/qwik-city';
import { input, buttonSecondary } from '../../data/class-list';
import type { DocumentHead } from '@builder.io/qwik-city';
import type { Event } from '../../models/Event';
import Card from '../../components/card/card';
import { GET } from '../../util/http';
import { EVENTS } from '../../data/url'
import { Loader } from '../../util/context';


export default component$(() => {
    const loader = useContext(Loader);

    const nav = useNavigate();
    const eventStore = useStore<{ events: Event[] }>({
        events: []
    }, {
        recursive: true
    })
    const navigate = $((id: string) => {
        nav.path = `/event/${id}`;
    });
    const filterEvents = $((val: string) => {
        eventStore.events.forEach(e => {
            if (!e.name.toLowerCase().includes(val.toLowerCase())) {
                e.hidden = true;
            } else {
                e.hidden = false;
            }
        })
    });
    useClientEffect$(async () => {
        try {
            const response: Event[] = await GET(EVENTS, loader);
            eventStore.events = response
        } catch (error) {
            console.log(error)
        }
    });
    return (
        <>
            <div class="flex justify-between mb-4">
                <input
                    class={`w-1/3 ${input}`}
                    placeholder='Search Events'
                    value=''
                    onInput$={(ev) => (filterEvents((ev.target as HTMLInputElement).value))}
                />
                <button class={buttonSecondary}
                    onClick$={() => { nav.path = '/event/null' }}>
                    Create Event
                </button>
            </div>
            <div class="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-5">
                {eventStore.events.filter(e => !e.hidden).map(event => (
                    <Card key={event._id} layout={event} click={navigate} />
                ))}
            </div>
        </>
    );
});

export const head: DocumentHead = {
    title: 'Event Dashboard'
};