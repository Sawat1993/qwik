import { component$, Slot, useContextProvider, useStore } from '@builder.io/qwik';
import Header from '../components/header/header';
import Footer from '../components/footer/footer';
import { Loader } from '../util/context';
import LoaderComponent from '../components/loader/loader';
import type { LoaderContext } from '../models/loader';

export default component$(() => {


  useContextProvider(
    Loader,
    useStore<LoaderContext>({
        show: false,
    })
);

  return (
    <>
      <main>
        <Header />
        <section>
          <LoaderComponent />
          <div class="relative bg-gray-100">
            <div class="mx-auto max-w-7xl px-4 sm:px-6">
              <div class='p-10'>
                <Slot />
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
});
