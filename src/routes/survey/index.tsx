import { component$, $, useStore } from '@builder.io/qwik';
import { buttonPrimary } from '../../data/class-list';
import Modal from '../../components/modal/modal'
import { randomStrings } from '../../data/random'

export default component$(() => {
    const store = useStore({
        primary: false,
        secondary: false,
        tertiary: false
    })

    const generateString = (x: number): string => {
        let retVal = '';
        for (let index = 0; index < x; index++) {
            const i = Math.round(Math.random() * randomStrings.length);
            retVal += `${randomStrings[i]} `

        }
        return retVal;
    }

    const submitPrimary = $(() => {
        store.primary = false
    });
    const rejectPrimary = $(() => {
        store.primary = false
    });
    const submitSecondary = $(() => {
        store.secondary = false
    });
    const rejectSecondary = $(() => {
        store.secondary = false
    });
    const submitTertiary = $(() => {
        store.tertiary = false
    });
    const rejectTertiary = $(() => {
        store.tertiary = false
    });
    return (
        <>
            <p class="text-center border-b-2 text-xl font-semibold p-2">Buttons</p>
            <div class="flex justify-center">
                <button class={buttonPrimary + ' m-5'}
                    onClick$={() => store.primary = true}>
                    Primary
                </button>
                {store.primary ? <Modal title={`Primary - ${generateString(3)}`} contents={[generateString(randomStrings.length / 2), generateString(randomStrings.length / 2)]}
                    acceptLabel="Accept" rejectLabel="Reject" submit={submitPrimary} reject={rejectPrimary} /> : <></>}
                <button class={buttonPrimary + ' m-5'}
                    onClick$={() => store.secondary = true}>
                    Secondary
                </button>
                {store.secondary ? <Modal title={`Secondary - ${generateString(3)}`} contents={[generateString(randomStrings.length / 2), generateString(randomStrings.length / 2)]}
                    acceptLabel="Accept" rejectLabel="Reject" submit={submitSecondary} reject={rejectSecondary} /> : <></>}
                <button class={buttonPrimary + ' m-5'}
                    onClick$={() => store.tertiary = true}>
                    Tertiary
                </button>
                {store.tertiary ? <Modal title={`Tertiary - ${generateString(3)}`} contents={[generateString(randomStrings.length / 2), generateString(randomStrings.length / 2)]}
                    acceptLabel="Accept" rejectLabel="Reject" submit={submitTertiary} reject={rejectTertiary} /> : <></>}

            </div>
        </>
    );
});
