import { component$, useStylesScoped$ } from '@builder.io/qwik';
import { Link } from '@builder.io/qwik-city';
import styles from './header.css?inline';
import { useLocation } from '@builder.io/qwik-city';

export default component$(() => {
  useStylesScoped$(styles);
  const loc = useLocation();
  const headers = [
    { name: 'Events', path: '/event/' },
    { name: 'Surveys', path: '/survey/' },
    { name: 'Blow my mind 🤯', path: '/flower/' }
  ];

  return (
    <div class="relative bg-blue-100 border-b-2 border-gray-100 py-6">
      <div class="mx-auto max-w-7xl px-4 sm:px-6">
        <div class="flex items-center justify-between">
          <a href="">
            <img class="h-8 w-auto sm:h-10" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt=""></img>
          </a>
          <nav class="hidden space-x-10 md:flex text-lg font-medium text-gray-500 ">
            {headers.map(header => (
              <Link href={header.path} key={header.name}
              class={loc.pathname.includes(header.path) ? 'text-gray-900 hover:text-gray-900' : 'hover:text-gray-900'}>{header.name}</Link>
            ))}
          </nav>
          <div>
            <img class="inline-block h-10 w-10 rounded-full ring-2 ring-white" src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""></img>
          </div>
        </div>
      </div>
    </div>
  );
});
