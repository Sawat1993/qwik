import { component$, useStore } from '@builder.io/qwik';
import type { Modal } from '../../models/modal';

export default component$((props: Modal) => {
    const store = useStore({
        show: true
    })

    return (
        <>
            {store.show ?
                <div id="defaultModal" aria-hidden="true" class="fixed h-screen w-screen z-[200] top-0 left-0 bg-blue-300/20">
                    <div class="flex justify-center items-center h-screen w-screen">
                        <div class="relative bg-white rounded-lg shadow dark:bg-gray-700 container mx-auto">
                            <div class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                                <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                                    {props?.title}
                                </h3>
                            </div>
                            <div class="p-6 space-y-6">
                                {props?.contents.map(content => (
                                    <p class="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                                        {content}
                                    </p>
                                ))}
                            </div>
                            <div class="flex items-center justify-end p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                                <button data-modal-hide="defaultModal" type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                    onClick$={async () => { await props.submit() }}>{props?.acceptLabel}</button>
                                {props?.rejectLabel ? <button data-modal-hide="defaultModal" type="button" class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                                    onClick$={async () => { await props.reject() }}>{props.rejectLabel}</button>
                                    : <></>}
                            </div>
                        </div>
                    </div>
                </div>
                : <></>}
        </>
    );
});
