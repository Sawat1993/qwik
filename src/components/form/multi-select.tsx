import { component$, useStore, $, useClientEffect$ } from "@builder.io/qwik";
import type { FormElement } from '../../models/Form'

export default component$((props: { layout: FormElement }) => {

    const focusInput = $(() => {
        const elem = document.getElementById(props.layout.key);
        if (elem) {
            elem.focus();
        }
    })
    const store = useStore({
        show: false,
        count: 0
    })

    const setCount = $(() => { store.count = props.layout.options?.filter(o => o.selected).length || 0 })

    useClientEffect$(() => {
        setCount();
    })
    return (
        <>
            <div class="shadow bg-white relative appearance-none border rounded py-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                <span class="absolute right-2 bg-green-100 text-green-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded-full dark:bg-green-200 dark:text-green-900">
                    {store.count}
                </span>
                <input
                    class="focus:outline-none w-full px-3"
                    id={props.layout.key}
                    placeholder={props.layout.placeholder}
                    value={props.layout.value}
                    onInput$={(ev) => (props.layout.value = (ev.target as HTMLInputElement).value)}
                    onFocusin$={() => { store.show = true }}
                    onFocusout$={() => { store.show = false }}
                />
                {store.show ? <div class="absolute bg-white w-full shadow mt-3 z-[100]"
                    onClick$={() => { focusInput() }}>
                    <ul class="overflow-y-auto px-3 pb-3 max-h-48">
                        {props.layout.options?.map(o => (
                            <li>
                                <div class="flex items-center px-2">
                                    <input type="checkbox"
                                        onChange$={(ev) => {
                                            o.selected = (ev.target as HTMLInputElement).checked;
                                            setCount()
                                            store.show = true;
                                        }}
                                        checked={o.selected} class="w-5 h-5 focus:outline-none" />
                                    <label class="py-2 ml-2 w-full">{`${o.label} (${o.value})`}</label>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div> : <></>}

            </div>
        </>
    )
})