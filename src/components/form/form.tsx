import { component$ } from '@builder.io/qwik';
import type { Form } from '../../models/Form';
import { label, input, buttonPrimary } from '../../data/class-list';
import MultiSelect from './multi-select';

export default component$((props: Form) => {
    return (
        <>
            <p class="font-bold text-xl mb-5">{props.title}</p>
            <div class="grid grid-cols-3 gap-x-20">
                {props.layout.map(element => (
                    <div class={`form-group mb-6 w-full ${element.width === 3 ? 'col-span-3' : element.width === 2 ? 'col-span-2' : 'col-span-1'}`}>
                        <label for={element.key} class={label}>{element.label}</label>
                        {
                            !element.type || element.type === 'text' || element.type === 'date' || element.type === 'time' ?
                                <input
                                    type={element.type}
                                    class={`${input} w-full`}
                                    placeholder={element.placeholder}
                                    value={element.value}
                                    onInput$={(ev) => (element.value = (ev.target as HTMLInputElement).value)}
                                /> :
                                element.type === 'textarea' ?
                                    <textarea
                                        class={`${input} w-full`}
                                        rows={3}
                                        value={element.value}
                                        placeholder={element.placeholder}
                                        onInput$={(ev) => (element.value = (ev.target as HTMLInputElement).value)}
                                    ></textarea> :
                                    element.type === 'dropdown' ?
                                        <select class={`${input} w-full`}
                                        onChange$={(ev) => (element.value = (ev.target as HTMLSelectElement).value)}>
                                            <option selected={!element.value}>{element.placeholder}</option>
                                            {element.options?.map(o => (
                                                <option value={o.value} selected={element.value === o.value}>{o.label}</option>
                                            ))}
                                        </select> :
                                        element.type === 'multi-dropdown' ? 
                                        <MultiSelect layout={element} /> :
                                        <>{` - ${element.type} - WIP`}</>
                        }</div>
                )
                )}
            </div>
            <div class="flex space-x-2 justify-center">
                <button class={buttonPrimary}
                    onClick$={async () => { await props.submit(props.layout) }}>
                    Submit
                </button>
            </div>
        </>
    );
});