import { component$ } from '@builder.io/qwik';
import type { Card } from '../../models/card';
export default component$((props : Card) => {
    return (
        <div class="rounded overflow-hidden shadow-lg cursor-pointer" onClick$={async () => {
            await props.click(props.layout._id)
            // nav.path = `/event/${event.id}`;
        }}>
            <img class="h-52 w-full" src={`/images/${props.layout.category}.jpg`} alt={props.layout.name}></img>
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">{props.layout.name}</div>
                <p class="text-gray-700 text-base">
                    {props.layout.desc}
                </p>
            </div>
            <div class="px-6 pt-4 pb-2">
                {props.layout.tag.map(tag => (
                    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
                        {tag}
                    </span>
                ))}
            </div>
        </div>
    );
});