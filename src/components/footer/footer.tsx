import { component$ } from '@builder.io/qwik';

export default component$(() => {

    const footerLink = [['facebook', 'twitter', 'linkedin'], ['facebook', 'twitter', 'linkedin'], ['facebook', 'twitter', 'linkedin']]

    return (
        <div class="relative bg-blue-100 border-t-2 border-gray-100 py-6 text-base font-medium text-gray-500">
            <div class="mx-auto max-w-7xl px-4 sm:px-6">
                <div class="flex flex-row items-center ">
                    <div class="basis-1/3">
                        Lorem ipsum may be used as a placeholder before final copy is available.
                    </div>
                    <div class="basis-2/3 flex flex-row-reverse">
                        {footerLink.map(links => (<div class="flex flex-col ml-4">{
                            links.map(link => (<a href="#" class="hover:text-gray-900">{link}</a>))
                        }</div>))}
                    </div>
                </div>
            </div>
        </div>
    );
});
