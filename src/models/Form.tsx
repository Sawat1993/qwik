import { PropFunction } from '@builder.io/qwik';

export interface FormElement {
    label?: string;
    value?: string;
    key: string;
    placeholder?: string;
    type?: 'text'|'dropdown'|'multi-dropdown'|'textarea'|'date'|'time';
    options?: {value: string, label: string, selected?: boolean}[];
    width?: 1|2|3;
}

export interface Form {
    layout: FormElement[];
    title?: string;
    submit: PropFunction<(form: FormElement[]) => void>
}