import { PropFunction } from '@builder.io/qwik';

export interface Modal {
    title: string;
    contents: string[];
    acceptLabel: string;
    rejectLabel?: string;
    submit: PropFunction<() => void>;
    reject: PropFunction<() => void>;
}