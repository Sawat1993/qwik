
export interface Event {
    _id: string;
    name: string;
    desc: string;
    category: string;
    tag: string[];
    hidden?: boolean;
}