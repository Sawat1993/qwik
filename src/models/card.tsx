import type { Event } from './Event';
import { PropFunction } from '@builder.io/qwik';

export interface Card {
    layout: Event,
    click: PropFunction<(id: string) => void>
}