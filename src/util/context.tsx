import { createContext } from "@builder.io/qwik";
import type { LoaderContext } from '../models/loader';
export const Loader = createContext<LoaderContext>('loader');
