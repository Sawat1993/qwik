import axios from "axios";
import type { LoaderContext } from '../models/loader'

async function post<T>(url: string, body: T, loader?: LoaderContext) {
    showLoader(loader);
    const data = await axios.post(url, body, { headers: { authorization: 'WIP' } });
    hideLoader(loader);
    return data;
}

async function get<T>(url: string, loader?: LoaderContext) {
    showLoader(loader);
    const data: { data: T } = await axios.get(url, { headers: { authorization: 'WIP' } });
    hideLoader(loader);
    return data.data;
}

async function put<T>(url: string, body: T, loader?: LoaderContext) {
    showLoader(loader);
    const data = await axios.put(url, body, { headers: { authorization: 'WIP' } });
    hideLoader(loader);
    return data;
}

function showLoader(loader?: LoaderContext) {
    if (loader) {
        loader.show = true;
    }
}

function hideLoader(loader?: LoaderContext) {
    if (loader) {
        loader.show = false;
    }
}

export const POST = post;
export const GET = get;
export const PUT = put;