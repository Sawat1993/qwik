import type {FormElement} from '../models/Form';

export const populateForm = (form: FormElement[], data: any): FormElement[] => {
    const f = JSON.parse(JSON.stringify(form));
    f.forEach((elem: FormElement) => {
        if(elem.type === 'multi-dropdown') {
            elem.options?.forEach(o => {
                if(data[elem.key].includes(o.value)){
                    o.selected = true;
                }});
        } else {
            elem.value = data[elem.key];
        }
    })
    return f;
}

export const getData = (form: FormElement[]) => {
    const data: any = {};
    form.forEach(elem => {
        if(elem.type === 'multi-dropdown') {
            data[elem.key] = elem.options?.filter(o => o.selected).map(o => o.value) || [];
        } else {
            data[elem.key] = elem.value;
        }
    })
    return data;
}