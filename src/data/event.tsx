
import type { FormElement } from '../models/Form';
export const form: FormElement[] = [{
    key: 'name',
    placeholder: 'Enter Module Name',
    label: 'Name:'
}, {
    key: 'images',
    placeholder: 'Enter Images URL',
    label: 'Images URL:'
}, {
    key: 'category',
    placeholder: 'Enter Category ',
    type: 'dropdown',
    options: [{ value: 'party', label: 'Party' },
    { value: 'lunch', label: 'Lunch' },
    { value: 'dinner', label: 'Dinner' },
    { value: 'breakfast', label: 'Breakfast' },
    { value: 'snacks', label: 'Snacks' }],
    label: 'Category:'
}, {
    key: 'tag',
    placeholder: 'Enter Tags ',
    type: 'multi-dropdown',
    options: [{ value: '#travel', label: 'Travel' },
    { value: '#nature', label: 'nature' },
    { value: '#pune', label: 'Pune' },
    { value: '#mumbai', label: 'mumbai' },
    { value: '#enjoy', label: 'enjoy' }],
    label: 'Tags:'
}, {
    key: 'members',
    placeholder: 'Enter Members ',
    type: 'multi-dropdown',
    // options: ['#xyz', '#travel', '#winter'],
    label: 'Members:',
    width: 2
}, {
    key: 'date',
    placeholder: 'Choose Date ',
    type: 'date',
    label: 'Date:',
}, {
    key: 'time',
    placeholder: 'Choose time ',
    type: 'time',
    label: 'Time:',
}, {
    key: 'place',
    placeholder: 'Enter Place Of Event',
    label: 'Place:'
}, {
    key: 'desc',
    type: 'textarea',
    placeholder: 'Enter Module Description',
    label: 'Description:',
    width: 3
}]