export const input = 'shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline';
export const label = 'form-label inline-block mb-1 text-gray-700';
export const buttonPrimary = 'bg-blue-700 hover:bg-blue-900  text-white font-semibold py-2 px-4 rounded';
export const buttonSecondary = 'bg-pink-500 hover:bg-pink-700 text-white font-semibold py-2 px-4 rounded';