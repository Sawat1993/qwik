const staticPaths = new Set(["/favicon.svg","/images/breakfast.jpg","/images/dinner.jpg","/images/lunch.jpg","/images/party.jpg","/images/snacks.jpg","/manifest.json","/q-manifest.json","/robots.txt","/service-worker.js"]);
function isStaticPath(p) {
  if (p.startsWith("/build/")) {
    return true;
  }
  if (p.startsWith("/assets/")) {
    return true;
  }
  if (staticPaths.has(p)) {
    return true;
  }
  return false;
}
export { isStaticPath };